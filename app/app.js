var express = require('express'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    MongoClient = require('mongodb').MongoClient,
    MongoDBStore = require('connect-mongodb-session')(session),
    data = require('gleaner-data').data,
    conf = require('./configuration');

// Configure service
var app = express();
app.set('view engine', 'jade');
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/public'));
app.use(cookieParser());

var store = new MongoDBStore(
    {
        uri: 'mongodb://' + conf.mongo.host + ':' + conf.mongo.port + '/' + conf.mongo.database,
        collection: 'sessions'
    });

app.use(session({
    secret: conf.sessionSecret,
    resave: false,
    saveUninitialized: false,
    store: store
}));

// Routes
var checkLogged = function (req, res, next) {
    if (req.session.role || req.path.startsWith(conf.collectorRoot) || req.path.startsWith(conf.loginPath)) {
        next();
    } else {
        res.redirect(conf.loginPath);
    }
};

// Add short delay, to see loading screens
if (conf.test) {
    app.all('*', function (req, res, next) {
        setTimeout(next, 200);
    });
}
app.all('*', checkLogged);

// Login
app.get(conf.loginPath, function (req, res) {
    if (req.session.role) {
        res.redirect('/');
    } else {
        res.render('login', {loginPath: conf.loginPath, query: req.query});
    }
});

app.post(conf.loginPath, bodyParser.urlencoded({extended: false}));

// API
app.post(conf.apiRoot + "*", bodyParser.json());

// Views
app.get('/', function (req, res) {
    switch (req.session.role) {
        case 'admin':
            res.redirect('/users');
            break;
        case 'developer':
            res.redirect('/games');
            break;
        case 'student':
            res.redirect('/course');
            break;
        default:
            res.send(404);
    }
});

app.get('/users', function (req, res) {
    res.render('users', {session: req.session});
});

app.get('/games', function (req, res) {
    res.render('games', {session: req.session});
});

app.get('/me', function (req, res) {
    res.render('me', {session: req.session});
});

app.get('/reports/:report', function (req, res) {
    var params = {session: req.session, query: req.query};
    res.render('reports/' + req.params.report, params, function (err, html) {
        if (err) {
            res.send(err.message);
        } else {
            params.html = html;
            res.render('report', params);
        }
    });
});

app.get('/course', function (req, res) {
    res.render('course', {session: req.session});
});

app.post(conf.collectorRoot + "*", bodyParser.text());


// Start service
var connect = function () {
    var mongoConf = conf.mongo;
    MongoClient.connect('mongodb://' + mongoConf.host + ':' + mongoConf.port + '/' + mongoConf.database, function (err, db) {
        if (err) {
            console.log('Error connecting to database:' + err);
            console.log('Reconnecting in 5 seconds');
            setTimeout(connect, 5000);
        } else {
            data(db, app, conf);
            app.listen(conf.port, function () {
                console.log('Listening in ' + conf.port);
            });
        }
    });
};

connect();

