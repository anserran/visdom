$(function () {
    $('#show-menu').click(function () {
        $('.sidebar').animate({
            left: "0"
        }, 900, 'easeOutExpo');
    });

    $('.sidebar').click(function(){
        $('.sidebar').animate({
            left: "-100%"
        }, 500, 'easeInCubic');
    });
});