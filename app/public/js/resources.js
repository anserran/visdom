var resources = function (name, options) {
    var app = angular.module('ResourcesApp', ['ngResource'])
        .factory('Resources', ['$resource',
            function ($resource) {
                return $resource('/api/' + name + '/:id', {
                    id: '@_id'
                });
            }]);

    if (options.onlyQuery) {
        app.controller('ResourcesCtrl', ['$scope', 'Resources',
            function ($scope, Resources) {
                $scope.resources = Resources.query();
            }]);
    } else {
        app.controller('ResourcesCtrl', ['$scope', 'Resources',
            function ($scope, Resources) {

                $scope.add = function () {
                    $scope.error = false;
                    $scope.adding = true;
                    $scope.resource = new Resources();
                    if (options.defaults) {
                        options.defaults($scope.resource);
                    }
                    $scope.show(false);
                };

                $scope.edit = function (resource) {
                    $scope.error = false;
                    $scope.adding = false;
                    $scope.resource = {};
                    _.extend($scope.resource, resource);
                    $scope.show(false);
                };

                $scope.show = function (list) {
                    if (list) {
                        $('#form').fadeOut(400, function () {
                            $('#list').fadeIn(400);
                        });
                    } else {
                        $('#list').fadeOut(400, function () {
                            $('#form').fadeIn(400);
                        });
                    }
                };

                $scope.cancel = function () {
                    $scope.show(true)
                };

                $scope.save = function () {
                    $scope.loading = true;
                    $scope.resource.$save(function () {
                        refresh();
                        $scope.show(true);
                    }, function (err) {
                        $scope.error = err.data;
                        $scope.loading = false;
                    });
                };

                $scope.delete = function () {
                    $scope.loading = true;
                    $scope.resource.$delete(refresh);
                    $scope.show(true)
                };

                var refresh = function () {
                    $scope.loading = true;
                    $scope.resources = Resources.query(function () {
                        $scope.loading = false;
                    });
                };

                refresh();
            }]);
    }
};

