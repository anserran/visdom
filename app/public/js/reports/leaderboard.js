var leaderboard = function (gameId) {
    angular.module('LeaderboardApp', ['ngResource'])
        .factory('Assessments', ['$resource',
            function ($resource) {
                return $resource('/api/players/assessments/' + gameId);
            }])
        .factory('Players', ['$resource',
            function ($resource) {
                return $resource('/api/players/:id', {
                    id: '@_id'
                });
            }])
        .factory('Users', ['$resource',
            function ($resource) {
                return $resource('/api/users/:id', {
                    id: '@_id'
                });
            }])
        .controller('LeaderboardCtrl', ['$scope', 'Assessments', 'Players', 'Users',
            function ($scope, Assessments, Players, Users) {

                var setNickname = function (assessment) {
                    assessment.player = Players.get({'id': assessment.playerId}, function () {
                        if (assessment.player.type === 'user') {
                            var user = Users.get({'id': assessment.player.userId}, function () {
                                assessment.player.name = user.nickname;
                            });
                        }
                    });
                };

                var update = function () {
                    var assessments = Assessments.query(function () {
                        for (var i = 0; i < assessments.length; i++) {
                            setNickname(assessments[i]);

                        }
                        $scope.assessments = assessments;
                        setTimeout(update, 10000);
                    });
                };
                update();
            }]);
};