var resource = function (name, options) {
    angular.module('ResourcesApp', ['ngResource'])
        .factory('Resources', ['$resource',
            function ($resource) {
                return $resource('/api/' + name + '/:id', {
                    id: '@_id'
                });
            }])
        .controller('ResourcesCtrl', ['$scope', 'Resources',
            function ($scope, Resources) {
                $scope.adding = false;

                $scope.save = function () {
                    $scope.loading = true;
                    if (typeof options.preSave === 'function'){
                        options.preSave($scope.resource);
                    }
                    $scope.resource.$save(function () {
                        refresh();
                    }, function (err) {
                        $scope.error = err.data;
                        $scope.loading = false;
                    });
                };

                if (options.delete) {
                    $scope.delete = function () {
                        $scope.loading = true;
                        $scope.resource.$delete(refresh);
                        $scope.show(true)
                    };
                }

                var refresh = function () {
                    $scope.loading = true;
                    $scope.resource = Resources.get({id: options.id}, function () {
                        $scope.loading = false;
                    });
                };

                refresh();
            }]);
};

