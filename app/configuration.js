var editUser = {
    create: false,
    read: {
        where: {
            _id: '$user'
        }
    },
    update: {
        where: {
            _id: '$user'
        },
        exclude: ['role', 'resources']
    },
    delete: false
};

var owner = {
    where: {
        owner: '$user'
    }
};


module.exports = {
    apiRoot: '/api/',
    collectorRoot: '/collect/',
    loginPath: '/login',
    redirectLogin: '/',
    passwordsSalt: 'somesalt',
    sessionSecret: 'somesecret',
    mongo: {
        host: "localhost",
        port: 27017,
        database: "gleaner"
    },
    port: 3000,
    roles: {
        admin: true,
        developer: {
            games: {
                create: 1,
                read: owner,
                update: owner,
                delete: owner
            },
            users: editUser
        },
        student: {
            users: editUser,
            games: {
                create: 0,
                read: {
                    where: {
                        visible: true
                    }
                }
            }
        }
    },
    test: true,
    kafka: {
        zookeeper: {
            host: 'localhost',
            port: 2181
        },
        topic: 'traces1'
    }
};