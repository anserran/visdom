if (process.argv.length != 3) {
    console.log('You must pass the path to a csv with the students');
} else {
    var MongoClient = require('mongodb').MongoClient,
        data = require('gleaner-data').data,
        conf = require('../app/configuration'),
        mongo = conf.mongo,
        Q = require('q'),
        users = require('gleaner-data').data,
        fs = require('fs');

    var path = process.argv[2];

    MongoClient.connect('mongodb://' + mongo.host + ':' + mongo.port + '/' + mongo.database, function (err, db) {
        if (err) {
            console.log(err);
        } else {
            users = data(db, null, conf);
            var lines = fs.readFileSync(path, 'utf8').split('\n');

            var promises = [];
            for (var i = 0; i < lines.length; i++) {
                var parts = lines[i].split(',');
                var newuser = {
                    name: parts[1],
                    nickname: parts[0],
                    password: parts[2],
                    role: 'student'
                };

                promises.push(users.collection().insert(newuser));
            }

            Q.all(promises).fail(function (err) {
                console.log(err);
            }).then(function () {
                console.log('Done');
                db.close();
            });
        }
    });
}