if (process.argv.length != 4) {
    console.log('You must pass the admin user and password as parameters');
} else {
    var MongoClient = require('mongodb').MongoClient,
        data = require('gleaner-data').data,
        conf = require('../app/configuration'),
        mongo = conf.mongo,
        users = require('gleaner-data').data;

    var user = process.argv[2];
    var password = process.argv[3];

    MongoClient.connect('mongodb://' + mongo.host + ':' + mongo.port + '/' + mongo.database, function (err, db) {
        if (err) {
            console.log(err);
        } else {
            users = data(db, null, conf);
            users.collection().insert({
                name: user,
                password: password,
                role: 'admin'
            }).then(function () {
                console.log('Done');
            }).fail(function (err) {
                console.log(err);
            }).then(function () {
                db.close();
            });
        }
    });
}